var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");

var app = express();  // make express app
var server = require('http').createServer(app); // inject app into the server
app.use(express.static(__dirname + '/assets'));
// 1 set up the view engine
app.set("views", path.resolve(__dirname, "views")) // path to views
app.set("view engine", "ejs") // specify our view engine

// 2 create an array to manage our entries
var entries = []
app.locals.entries = entries // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"))     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }))

// 4 handle http GET requests (default & /new-entry)
app.get("/", function (request, response) {
  response.sendFile(__dirname+"/pages/about.html");
});
app.get("/about", function (request, response) {
  response.sendFile(__dirname+"/pages/about.html");
});
app.get("/contacts", function (request, response) {
  response.sendFile(__dirname+"/views/contacts.html");
});
app.get("/gpa_calc", function (request, response) {
  response.sendFile(__dirname+"/pages/gpa_calc.html");
});
app.get("/guestbook", function (request, response) {
  response.render("index")
})
app.get("/new-entry", function (request, response) {
  response.render("new-entry")
});


// POSTS
// 5 handle an http POST request to the new-entry URI 
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.")
    return
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  })
  response.redirect("/guestbook")  // where to go next? Let's go to the home page :)
});

app.post("/contacts", function(request,response){
  console.log("checking mail")
  var api_key = 'key-770c3cc90056cea80af1cafa3f4079cb';
  var domain = 'sandbox520a2a03c8ae42dcb83afd7b3e7ffdad.mailgun.org';
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: 'Mail Gun <postmaster@sandbox520a2a03c8ae42dcb83afd7b3e7ffdad.mailgun.org>',
    to: 'S530489@nwmissouri.edu',
    subject: "Mail from "+ request.body.firstname,
    text: "Full Name: "+request.body.firstname+", "+request.body.lastname+
    "\n\nDate of Birth: "+request.body.bday+"\n\nEmail:"+request.body.email+
    "\n\nAny questions: "+request.body.Questions+"\n\nFeedback: "+request.body.message
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    if(!error)
      response.send("mail sent sucessfully");
    else
      response.send("mail not sent");

  });
});

// 404
// if we get a 404 status, render our 404.ejs view
app.use(function (request, response) {
  response.status(404).render("404")
});

// Listen for an application request on port 8081
server.listen(8081, function () {
  console.log('App is listening on http://127.0.0.1:8081/');
});
