# A03 Adding server side functionality to application

My project(A03Uppla) contains 4 web pages
1. uppala_saikumar.html - It is the home page which introduces me. It contains video and image.
2. gpa_calc.html - It takes inputs from the user for grades and credits and calculates the gpa.
3. contact.html - It is the contact page where users can enter their details,ask questions,feedback and submit which         triggers a mail.
4. A simple guest book using Node, Express, BootStrap, EJS.


## How to use

Open a command window in your c:\44563\A03 folder.

Run npm install to install all the dependencies in the package.json file.

Run node gbapp.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node gbapp.js
```

Point your browser to `http://localhost:8081`.